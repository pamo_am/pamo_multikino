package pl.edu.pjwstk.pamo_multikino;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageButton;

import com.squareup.picasso.Picasso;

import java.util.List;

public class SlideAdapter extends PagerAdapter {
    Context context;
    LayoutInflater inflater;
    private List<Movie> list;

    private int position_of_slider;

    public SlideAdapter(Context context, List<Movie> list)
    {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        if(list.size()>0)
            return list.size();
        else
            return 0;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        int size = list.size();

        final String[] titles = new String[size];
        String[] posters = new String[size];
        for(int i =0; i< size;i++) {
            titles[i] = list.get(i).getTitle();
            posters[i] = "https:"+list.get(i).getPosterUrl();
        }

        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide,container,false);
        LinearLayout layoutslide = view.findViewById(R.id.slidelinearlayout);
        ImageView imgslide = (ImageView) view.findViewById(R.id.slideimg);
        TextView txttitle = (TextView) view.findViewById(R.id.txttitle);
        Picasso.get().load(posters[position]).into(imgslide);
        txttitle.setText(titles[position]);

        ImageButton mapsButton = (ImageButton) view.findViewById(R.id.mapsButton);
        ImageButton youtubeButton = (ImageButton) view.findViewById(R.id.youtubeButton);

        mapsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("https://www.google.com/maps/dir/?api=1&destination=Multikino+Gdansk&travelmode=driving"));
                Intent chooseIntent = Intent.createChooser(intent, "Open with");
                chooseIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(chooseIntent);
            }
        });

        youtubeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = new String();
                url="https://www.youtube.com/results?search_query="+titles[position];

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse(url));
                Intent chooseIntent = Intent.createChooser(intent, "Open with");
                chooseIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(chooseIntent);
            }
        });

        position_of_slider = position - 1;

        imgslide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),DisplayMessageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("movieId", list.get(position_of_slider).getId());
                v.getContext().startActivity(intent);
            }
        });

        container.addView(view);

        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return (view==(LinearLayout)o);
    }

}