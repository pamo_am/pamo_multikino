package pl.edu.pjwstk.pamo_multikino;

import java.util.ArrayList;
import java.util.List;

public class Movie {
    private String id;
    private String title;
    private String titleOriginal;
    private String posterUrl;
    private List<String> trailersUrls = new ArrayList<>();
    private List<String> imagesUrls = new ArrayList<>();
    private String movieAbstract;
    private String description;
    private String shortDescription;
    private String runtime;
    private String country;
    private String direction;
    private String cast;
    private String genres;
    private Long occurrences;
    private Double rate = 0.0;
    private String premiere;
    private String filmwebLink = "";

    public Movie(String id, String title, String posterUrl, String shortDescription) {
        this.id = id;
        this.title = title;
        this.posterUrl = posterUrl;
        this.shortDescription = shortDescription;
    }

    public Movie(){}

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTitleOriginal(String titleOriginal) {
        this.titleOriginal = titleOriginal;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public void setTrailersUrls(List<String> trailersUrls) {
        this.trailersUrls = trailersUrls;
    }

    public void setImagesUrls(List<String> imagesUrls) {
        this.imagesUrls = imagesUrls;
    }

    public void setMovieAbstract(String movieAbstract) {
        this.movieAbstract = movieAbstract;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public void setOccurrences(Long occurrences) {
        this.occurrences = occurrences;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public void setPremiere(String premiere) {
        this.premiere = premiere;
    }

    public void setFilmwebLink(String filmwebLink) {
        this.filmwebLink = filmwebLink;
    }


    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleOriginal() {
        return titleOriginal;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public List<String> getTrailersUrls() {
        return trailersUrls;
    }

    public List<String> getImagesUrls() {
        return imagesUrls;
    }

    public String getMovieAbstract() {
        return movieAbstract;
    }

    public String getDescription() {
        return description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getCountry() {
        return country;
    }

    public String getDirection() {
        return direction;
    }

    public String getCast() {
        return cast;
    }

    public String getGenres() {
        return genres;
    }

    public Long getOccurrences() {
        return occurrences;
    }

    public Double getRate() {
        return rate;
    }

    public String getPremiere() {
        return premiere;
    }

    public String getFilmwebLink() {
        return filmwebLink;
    }
}

