package pl.edu.pjwstk.pamo_multikino;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ShowingListAdapter extends ArrayAdapter<Showing> {

    private static final String TAG = "ShowingListAdapter";

    private Context mContext;
    private int mResource;
    private int lastPosition = -1;

    /**
     * Holds variables in a View
     */
    private static class ViewHolder {
        TextView eventTime;
        TextView fillinCinemaHall;
        TextView cinema;
    }

    /**
     * Default constructor for the ShowingListAdapter
     * @param context
     * @param resource
     * @param objects
     */
    public ShowingListAdapter(Context context, int resource, ArrayList<Showing> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //get the showing information
        String eventTime = getItem(position).getEventTime();
        String fillinCinemaHall = getItem(position).getFillinCinemaHall();
        String cinemaName = getItem(position).getCinemaName();
        String cinemaCity = getItem(position).getCinemaCity();
        String link = getItem(position).getLink();

        //Create the showing object with the information
        final Showing showing = new Showing(eventTime, fillinCinemaHall, cinemaName, cinemaCity, link);

        //create the view result for showing the animation
        final View result;

        //ViewHolder object
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);
            holder= new ViewHolder();
            holder.eventTime = (TextView) convertView.findViewById(R.id.textView1);
            holder.fillinCinemaHall = (TextView) convertView.findViewById(R.id.textView2);
            holder.cinema = (TextView) convertView.findViewById(R.id.textView3);

            result = convertView;

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
        result.startAnimation(animation);
        lastPosition = position;

        holder.eventTime.setText(showing.getEventTime());
        holder.fillinCinemaHall.setText(showing.getFillinCinemaHall());
        holder.cinema.setText(showing.getCinemaName() + " " + showing.getCinemaCity());

        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = showing.getLink();

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse(url));
                Intent chooseIntent = Intent.createChooser(intent, "Open with");
                chooseIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(chooseIntent);
            }
        });

        return convertView;
    }
}