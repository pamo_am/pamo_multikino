package pl.edu.pjwstk.pamo_multikino;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import static pl.edu.pjwstk.pamo_multikino.Localhost.LOCALHOST_IP;

public class DisplayMessageActivity extends AppCompatActivity {

    private ArrayList<Showing> showingList;

    ListView mListView;

    private String movieId;

    String url = "http://" + LOCALHOST_IP + ":8080/repertoire";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Button mBtn = (Button)findViewById(R.id.button);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DisplayMessageActivity.this, MainActivity.class));
            }
        });

        mListView = (ListView) findViewById(R.id.listView1);

        showingList = new ArrayList<>();

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            movieId = extras.getString("movieId");
        }

        getDataForShowings(movieId);

        ShowingListAdapter adapter = new ShowingListAdapter(this, R.layout.showings2, showingList);
        mListView.setAdapter(adapter);

    }

    private void getDataForShowings(String movieId){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Ładuję...");
        progressDialog.show();

        final String movieIdFromPoster = movieId;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        Showing showing = new Showing();
                        showing.setId(jsonObject.getString("id"));

                        String evenTimePureString = jsonObject.getString("eventTime");
                        Integer eventTimeHour = Integer.parseInt(evenTimePureString.substring(11,13)) + 2;
                        String eventTime = eventTimeHour.toString() + evenTimePureString.substring(13,16);

                        showing.setEventTime(eventTime);
                        showing.setCinemaName(jsonObject.getString("cinemaName"));
                        showing.setCinemaCity(jsonObject.getString("cinemaCity"));
                        showing.setLink(jsonObject.getString("link"));

                        if(jsonObject.getString("fillinCinemaHall").equals("null")) {
                            Random rand = new Random();
                            Integer randomNumber = rand.nextInt((10 - 1) + 1) + 1;

                            showing.setFillinCinemaHall("Zapełnienie sali: " + randomNumber.toString() + " %");
                        } else {
                            showing.setFillinCinemaHall("Zapełnienie sali: " + jsonObject.getString("fillinCinemaHall") + " %");
                        }

                        showing.setMovieId(jsonObject.getString("movieId"));

                        if(showing.getMovieId().equals(movieIdFromPoster)) {
                            showingList.add(showing);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

                progressDialog.dismiss();

                ShowingListAdapter adapter = new ShowingListAdapter(getApplicationContext(), R.layout.showings2, showingList);
                mListView.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

}
