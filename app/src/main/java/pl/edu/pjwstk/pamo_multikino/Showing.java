package pl.edu.pjwstk.pamo_multikino;

public class Showing {

    private String id;
    private String eventTime;
    private String cinemaName;
    private String cinemaCity;
    private String link;
    private String fillinCinemaHall;
    private String movieId;

    public Showing() {};

    public Showing(String eventTime, String fillinCinemaHall, String cinemaName, String cinemaCity, String link) {
        this.id = id;
        this.eventTime = eventTime;
        this.cinemaName = cinemaName;
        this.cinemaCity = cinemaCity;
        this.link = link;
        this.fillinCinemaHall = fillinCinemaHall;
    }

    public Showing(String id, String eventTime, String cinemaName, String cinemaCity, String link, String fillinCinemaHall) {
        this.id = id;
        this.eventTime = eventTime;
        this.cinemaName = cinemaName;
        this.cinemaCity = cinemaCity;
        this.link = link;
        this.fillinCinemaHall = fillinCinemaHall;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getCinemaCity() {
        return cinemaCity;
    }

    public void setCinemaCity(String cinemaCity) {
        this.cinemaCity = cinemaCity;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFillinCinemaHall() {
        return fillinCinemaHall;
    }

    public void setFillinCinemaHall(String fillinCinemaHall) {
        this.fillinCinemaHall = fillinCinemaHall;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    @Override
    public String toString() {
        return "Showing{" +
                "id='" + id + '\'' +
                ", eventTime='" + eventTime + '\'' +
                ", cinemaName='" + cinemaName + '\'' +
                ", cinemaCity='" + cinemaCity + '\'' +
                ", link='" + link + '\'' +
                ", fillinCinemaHall='" + fillinCinemaHall + '\'' +
                ", movieId='" + movieId + '\'' +
                '}';
    }
}